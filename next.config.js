// next.config.js
const withPlugins = require('next-compose-plugins')
const withImages = require('next-images') //Next.js image importer
const withSass = require('@zeit/next-sass') //Next.js sass

module.exports = withPlugins([
    [withImages],
    [withSass]
])