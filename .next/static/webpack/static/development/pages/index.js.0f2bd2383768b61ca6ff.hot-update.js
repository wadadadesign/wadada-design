webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/global/globalStyles.js":
/*!*******************************************!*\
  !*** ./components/global/globalStyles.js ***!
  \*******************************************/
/*! exports provided: siteColours, siteFonts, transitions, opacityReveal, titleReveal, FlexContainer, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "siteColours", function() { return siteColours; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "siteFonts", function() { return siteFonts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "transitions", function() { return transitions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "opacityReveal", function() { return opacityReveal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "titleReveal", function() { return titleReveal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FlexContainer", function() { return FlexContainer; });
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral */ "./node_modules/@babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");


function _templateObject4() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\nbody {\n    height: 100vh;\n    overflow: hidden;\n    position: relative;\n    color: ", ";\n    margin: 0;\n    font-family: ", ";\n    font-weight: 400;\n    font-size: 21px;\n\n    @media (max-width: 576px) {\n        overflow: scroll;\n    }\n}\n\nh1, h2, h3, h4, h5 {\n  font-weight: 500;\n}\n\nh1 {\n    font-size: 3.5vw;\n\n    @media (max-width: 1024px) {\n        font-size: 5.5vw;\n    }\n\n    @media (max-width: 576px) {\n        font-size: 32px;\n    }\n}\n\nh2 {\n    font-size: 2.6vw;\n\n    @media (max-width: 1024px) {\n        font-size: 3.5vw;\n    }\n\n    @media (max-width: 576px) {\n        font-size: 24px;\n        line-height: 1.5;\n    }\n}\n\n\nh1, h2, h3, h4, h5, p {\n    margin: 0 0 40px;\n\n    @media (max-width: 1200px) {\n        margin: 0 0 25px;\n    }\n}\n\np {\n    color: ", ";\n    line-height: 1.75;\n    font-size: 1vw;\n\n    @media (max-width: 1024px) {\n        font-size: 2vw;\n    }\n\n    @media (max-width: 576px) {\n        font-size: 21px;\n    }\n}\n\nbutton, input, textarea, select {\n    font-family: ", "\n}\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    display: flex;\n    overflow: hidden;\n    position: relative;\n    height: ", ";\n    flex-direction: ", ";\n    justify-content: ", ";\n    padding: ", ";\n\n    @media (max-width: 576px) {\n        height: auto;\n    }\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    from {transform: translateY(20px); opacity: 0;}\n    to {transfrom: translateY(0); opacity: 1;}\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    from {opacty: 0;}\n    to {opacity: 1;}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

 //Site wide variables

var siteColours = {
  black: "#111",
  blue: "#0f1dff",
  lightGrey: "#525252"
};
var siteFonts = {
  display: "objektiv-mk1, sans-serif;"
};
var transitions = {
  default: "0.3s ease",
  cubicBezier: {
    default: "cubic-bezier(.9,0,.7,1)",
    soft: "cubic-bezier(.33,.31,.21,1)"
  },
  delay: {
    first: "1s",
    second: "2s",
    third: "3s",
    fourth: "4s"
  }
};
var opacityReveal = Object(styled_components__WEBPACK_IMPORTED_MODULE_1__["keyframes"])(_templateObject());
var titleReveal = Object(styled_components__WEBPACK_IMPORTED_MODULE_1__["keyframes"])(_templateObject2());
var FlexContainer = styled_components__WEBPACK_IMPORTED_MODULE_1__["default"].section(_templateObject3(), function (props) {
  return props.height || "auto";
}, function (props) {
  return props.flexDirection || "flex-start";
}, function (props) {
  return props.justifyContent || "flex-start";
}, function (props) {
  return props.padding || "none";
}); //Gloabl styles

/* harmony default export */ __webpack_exports__["default"] = (Object(styled_components__WEBPACK_IMPORTED_MODULE_1__["createGlobalStyle"])(_templateObject4(), siteColours.black, siteFonts.display, siteColours.lightGrey, siteFonts.display));

/***/ })

})
//# sourceMappingURL=index.js.0f2bd2383768b61ca6ff.hot-update.js.map