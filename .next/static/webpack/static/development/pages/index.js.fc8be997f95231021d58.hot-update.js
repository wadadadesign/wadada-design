webpackHotUpdate("static/development/pages/index.js",{

/***/ "./components/ui/buttons.jsx":
/*!***********************************!*\
  !*** ./components/ui/buttons.jsx ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral */ "./node_modules/@babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _global_globalStyles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../global/globalStyles */ "./components/global/globalStyles.js");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");

var _jsxFileName = "/Users/giladgrant/Documents/Wadada/Internal/Development/wadada-design/components/ui/buttons.jsx";


function _templateObject2() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        background: white;\n        color: ", ";\n        transition: ", ";\n\n        :hover {\n            background: ", ";\n            color: white;\n            transform: translateY(3px);\n        }\n    "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    font-size: 0.8vw;\n    text-transform: uppercase;\n    padding: 20px 40px;\n    border-radius: 50px;\n    cursor: pointer;\n    font-weight: 500;\n    border: none;\n    box-shadow: 0 5px 10px 0 rgba(7, 40, 251, 0.21);\n    display: inline-block;\n    text-decoration: none;\n\n    @media (max-width: 1024px) {\n        font-size: 12px;\n        padding: 15px 30px;\n    }\n\n    ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}



var StyledButton = styled_components__WEBPACK_IMPORTED_MODULE_3__["default"].a(_templateObject(), function (props) {
  return props.buttonStyle == 'primary' && Object(styled_components__WEBPACK_IMPORTED_MODULE_3__["css"])(_templateObject2(), _global_globalStyles__WEBPACK_IMPORTED_MODULE_2__["siteColours"].blue, _global_globalStyles__WEBPACK_IMPORTED_MODULE_2__["transitions"].default, _global_globalStyles__WEBPACK_IMPORTED_MODULE_2__["siteColours"].blue);
});

var Button = function Button(props) {
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(StyledButton, {
    buttonStyle: props.buttonStyle,
    href: props.to,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: this
  }, props.children);
};

/* harmony default export */ __webpack_exports__["default"] = (Button);

/***/ })

})
//# sourceMappingURL=index.js.fc8be997f95231021d58.hot-update.js.map