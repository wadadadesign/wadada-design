module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/global/globalStyles.js":
/*!*******************************************!*\
  !*** ./components/global/globalStyles.js ***!
  \*******************************************/
/*! exports provided: siteColours, siteFonts, transitions, opacityReveal, titleReveal, FlexContainer, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "siteColours", function() { return siteColours; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "siteFonts", function() { return siteFonts; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "transitions", function() { return transitions; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "opacityReveal", function() { return opacityReveal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "titleReveal", function() { return titleReveal; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FlexContainer", function() { return FlexContainer; });
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral */ "./node_modules/@babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_1__);


function _templateObject4() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\nbody {\n    height: 100vh;\n    overflow: hidden;\n    position: relative;\n    color: ", ";\n    margin: 0;\n    font-family: ", ";\n    font-weight: 400;\n    font-size: 21px;\n\n    @media (max-width: 576px) {\n        overflow: scroll;\n    }\n}\n\nh1, h2, h3, h4, h5 {\n  font-weight: 500;\n}\n\nh1 {\n    font-size: 3.5vw;\n\n    @media (max-width: 1024px) {\n        font-size: 5.5vw;\n    }\n\n    @media (max-width: 576px) {\n        font-size: 32px;\n    }\n}\n\nh2 {\n    font-size: 2.6vw;\n\n    @media (max-width: 1024px) {\n        font-size: 3.5vw;\n    }\n\n    @media (max-width: 576px) {\n        font-size: 24px;\n        line-height: 1.5;\n    }\n}\n\n\nh1, h2, h3, h4, h5, p {\n    margin: 0 0 40px;\n\n    @media (max-width: 1200px) {\n        margin: 0 0 25px;\n    }\n}\n\np {\n    color: ", ";\n    line-height: 1.75;\n    font-size: 1vw;\n\n    @media (max-width: 1024px) {\n        font-size: 2vw;\n    }\n\n    @media (max-width: 576px) {\n        font-size: 21px;\n    }\n}\n\nbutton, input, textarea, select {\n    font-family: ", "\n}\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    display: flex;\n    overflow: hidden;\n    position: relative;\n    height: ", ";\n    flex-direction: ", ";\n    justify-content: ", ";\n    padding: ", ";\n\n    @media (max-width: 576px) {\n        height: auto;\n    }\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    from {transform: translateY(20px); opacity: 0;}\n    to {transfrom: translateY(0); opacity: 1;}\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    from {opacty: 0;}\n    to {opacity: 1;}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

 //Site wide variables

var siteColours = {
  black: "#111",
  blue: "#0f1dff",
  lightGrey: "#525252"
};
var siteFonts = {
  display: "objektiv-mk1, sans-serif;"
};
var transitions = {
  default: "0.3s ease",
  cubicBezier: {
    default: "cubic-bezier(.9,0,.7,1)",
    soft: "cubic-bezier(.33,.31,.21,1)"
  },
  delay: {
    first: "1s",
    second: "2s",
    third: "3s",
    fourth: "4s"
  }
};
var opacityReveal = Object(styled_components__WEBPACK_IMPORTED_MODULE_1__["keyframes"])(_templateObject());
var titleReveal = Object(styled_components__WEBPACK_IMPORTED_MODULE_1__["keyframes"])(_templateObject2());
var FlexContainer = styled_components__WEBPACK_IMPORTED_MODULE_1___default.a.section(_templateObject3(), function (props) {
  return props.height || "auto";
}, function (props) {
  return props.flexDirection || "flex-start";
}, function (props) {
  return props.justifyContent || "flex-start";
}, function (props) {
  return props.padding || "none";
}); //Gloabl styles

/* harmony default export */ __webpack_exports__["default"] = (Object(styled_components__WEBPACK_IMPORTED_MODULE_1__["createGlobalStyle"])(_templateObject4(), siteColours.black, siteFonts.display, siteColours.lightGrey, siteFonts.display));

/***/ }),

/***/ "./components/global/layout.jsx":
/*!**************************************!*\
  !*** ./components/global/layout.jsx ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _global_globalStyles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../global/globalStyles */ "./components/global/globalStyles.js");
var _jsxFileName = "/Users/giladgrant/Documents/Wadada/Internal/Development/wadada-design/components/global/layout.jsx";




var Layout = function Layout(props) {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0___default.a.Fragment, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_head__WEBPACK_IMPORTED_MODULE_1___default.a, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    charSet: "utf-8",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "viewport",
    content: "width=device-width, initial-scale=1",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("link", {
    rel: "stylesheet",
    href: "https://use.typekit.net/ihj0vor.css",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("link", {
    rel: "manifest",
    href: "/static/manifest.json",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("link", {
    rel: "shortcut icon",
    href: "/static/favicon.ico",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("main", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_global_globalStyles__WEBPACK_IMPORTED_MODULE_2__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: this
  }), props.children));
};

/* harmony default export */ __webpack_exports__["default"] = (Layout);

/***/ }),

/***/ "./components/home/caption.jsx":
/*!*************************************!*\
  !*** ./components/home/caption.jsx ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral */ "./node_modules/@babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ui_buttons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../ui/buttons */ "./components/ui/buttons.jsx");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _global_globalStyles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../global/globalStyles */ "./components/global/globalStyles.js");

var _jsxFileName = "/Users/giladgrant/Documents/Wadada/Internal/Development/wadada-design/components/home/caption.jsx";


function _templateObject3() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\nanimation-delay: ", ";\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\nopacity: 0;\ndisplay: inline-block;\nanimation: ", " 1s ease 1 forwards;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    padding-right: 10%;\n\n    @media (max-width: 1024px) {\n        padding: 0;\n    }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}




var CaptionContainer = styled_components__WEBPACK_IMPORTED_MODULE_3___default.a.div(_templateObject());
var AnimatedTag = styled_components__WEBPACK_IMPORTED_MODULE_3___default.a.span(_templateObject2(), _global_globalStyles__WEBPACK_IMPORTED_MODULE_4__["titleReveal"]);
var AnimatedTagDelayed = styled_components__WEBPACK_IMPORTED_MODULE_3___default()(AnimatedTag)(_templateObject3(), _global_globalStyles__WEBPACK_IMPORTED_MODULE_4__["transitions"].delay.first);

var Caption = function Caption() {
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(CaptionContainer, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 24
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h1", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(AnimatedTag, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }, "Wadada! I'm Gil")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(AnimatedTagDelayed, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("h2", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, "I'm a web designer and developer based in Birmingham, UK"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: this
  }, "I am passionate about clean design and creating modern web experiences... oh, and football. If you have a problem you need solving, I'm always free for a chat, especially if it's about websites."), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_ui_buttons__WEBPACK_IMPORTED_MODULE_2__["default"], {
    buttonStyle: "primary",
    to: "mailto:gilad@wadada-design.com?subject=What a lovely site you have!",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: this
  }, "Let's chat")));
};

/* harmony default export */ __webpack_exports__["default"] = (Caption);

/***/ }),

/***/ "./components/home/socialLinks.jsx":
/*!*****************************************!*\
  !*** ./components/home/socialLinks.jsx ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral */ "./node_modules/@babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_icons_fa__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-icons/fa */ "react-icons/fa");
/* harmony import */ var react_icons_fa__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_icons_fa__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _global_globalStyles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../global/globalStyles */ "./components/global/globalStyles.js");

var _jsxFileName = "/Users/giladgrant/Documents/Wadada/Internal/Development/wadada-design/components/home/socialLinks.jsx";


function _templateObject() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\ndisplay: flex;\nopacity: 0;\nanimation: ", " 2s ", " ease forwards;\n        \n>*:not(:last-of-type) {\n    margin-right: 30px;\n}\n\nsvg {\n    color: ", "\n    height: 1.5em;\n    width: auto;\n\n    @media (max-width: 1600px) {\n        height: 20px;\n    }\n}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}




var socialLinks = {
  dribble: "https://dribbble.com/wadadadesign",
  instagram: "https://instagram.com/wadadadesign"
};
var SocialLinksContainer = styled_components__WEBPACK_IMPORTED_MODULE_3___default.a.div(_templateObject(), _global_globalStyles__WEBPACK_IMPORTED_MODULE_4__["opacityReveal"], _global_globalStyles__WEBPACK_IMPORTED_MODULE_4__["transitions"].delay.third, _global_globalStyles__WEBPACK_IMPORTED_MODULE_4__["siteColours"].blue);

var SocialLinks = function SocialLinks() {
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(SocialLinksContainer, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: socialLinks.dribble,
    rel: "noopener",
    target: "_blank",
    title: "Check me out on Dribbble",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_icons_fa__WEBPACK_IMPORTED_MODULE_2__["FaDribbble"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
    href: socialLinks.instagram,
    rel: "noopener",
    target: "_blank",
    title: "Check me out on Instagram",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_icons_fa__WEBPACK_IMPORTED_MODULE_2__["FaInstagram"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  })));
};

/* harmony default export */ __webpack_exports__["default"] = (SocialLinks);

/***/ }),

/***/ "./components/home/wave.jsx":
/*!**********************************!*\
  !*** ./components/home/wave.jsx ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral */ "./node_modules/@babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_svg__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-svg */ "react-svg");
/* harmony import */ var react_svg__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_svg__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _global_globalStyles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../global/globalStyles */ "./components/global/globalStyles.js");

var _jsxFileName = "/Users/giladgrant/Documents/Wadada/Internal/Development/wadada-design/components/home/wave.jsx";


function _templateObject4() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\nposition: relative;\nheight: 100%;\n\n&::after {\n    content: '';\n    position: absolute;\n    top: 0;\n    right: 0;\n    width: 100%;\n    height: 100%;\n    z-index: 10;\n    background: white;\n    animation: ", " .5s ", " ", " 1 forwards;\n}\n\nsvg {\n    position: absolute;\n    transform: rotate(30deg) scale(2);\n    pointer-events: none;\n    height: 100%;\n    opacity: 0;\n    animation: ", " 3s ease ", " 1 forwards;\n\n    path {\n      animation: ", " 20s ease-in-out infinite alternate;\n      stroke: white;\n      stroke-width: 10 !important;\n\n      @media (max-width: 1024px) {\n          opacity: 0.01;\n        stroke: ", ";\n        }\n    }\n\n    ", "\n  }\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["", ""]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    from {\n        width: 100%;\n    }\n    to {\n        width: 0%;\n    }\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n0% {transform: translate3D(0%, 0, 0);}\n50% {transform: translate3D(-150%, 0, 0);}\n100% {transform: translate3D(0, 0, 0);}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}



 //Wave animation

var waveMove = Object(styled_components__WEBPACK_IMPORTED_MODULE_2__["keyframes"])(_templateObject()); //White overlay animation

var overlayReveal = Object(styled_components__WEBPACK_IMPORTED_MODULE_2__["keyframes"])(_templateObject2()); //Create delay in wave animation

var waveAnimationDelay = function waveAnimationDelay() {
  var styles = ''; //Loop and increase animation delay by 0.1 each time

  for (var i = 1; i < 30; i++) {
    styles += "\n            path:nth-of-type(".concat(i, ") {\n                animation-delay: ").concat(i * 0.1, "s;\n            }\n        ");
  } //`css` function 


  return Object(styled_components__WEBPACK_IMPORTED_MODULE_2__["css"])(_templateObject3(), styles);
}; //Styles for the SVG


var OuterSVG = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.div(_templateObject4(), overlayReveal, _global_globalStyles__WEBPACK_IMPORTED_MODULE_4__["transitions"].cubicBezier.default, _global_globalStyles__WEBPACK_IMPORTED_MODULE_4__["transitions"].delay.first, _global_globalStyles__WEBPACK_IMPORTED_MODULE_4__["opacityReveal"], _global_globalStyles__WEBPACK_IMPORTED_MODULE_4__["transitions"].delay.second, waveMove, _global_globalStyles__WEBPACK_IMPORTED_MODULE_4__["siteColours"].blue, waveAnimationDelay()); //WaveSVG component

var WaveSVG = function WaveSVG() {
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(OuterSVG, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_svg__WEBPACK_IMPORTED_MODULE_3___default.a, {
    src: "../static/Test.svg",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 80
    },
    __self: this
  }));
};

/* harmony default export */ __webpack_exports__["default"] = (WaveSVG);

/***/ }),

/***/ "./components/ui/buttons.jsx":
/*!***********************************!*\
  !*** ./components/ui/buttons.jsx ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral */ "./node_modules/@babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _global_globalStyles__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../global/globalStyles */ "./components/global/globalStyles.js");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_3__);

var _jsxFileName = "/Users/giladgrant/Documents/Wadada/Internal/Development/wadada-design/components/ui/buttons.jsx";


function _templateObject2() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n        background: white;\n        color: ", ";\n        transition: ", ";\n\n        :hover {\n            background: ", ";\n            color: white;\n            transform: translateY(3px);\n        }\n    "]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    font-size: 0.8vw;\n    text-transform: uppercase;\n    padding: 20px 40px;\n    border-radius: 50px;\n    cursor: pointer;\n    font-weight: 500;\n    border: none;\n    box-shadow: 0 5px 10px 0 rgba(7, 40, 251, 0.21);\n    display: inline-block;\n    text-decoration: none;\n\n    @media (max-width: 1024px) {\n        font-size: 12px;\n        padding: 15px 30px;\n    }\n\n    ", "\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}



var StyledButton = styled_components__WEBPACK_IMPORTED_MODULE_3___default.a.a(_templateObject(), function (props) {
  return props.buttonStyle == 'primary' && Object(styled_components__WEBPACK_IMPORTED_MODULE_3__["css"])(_templateObject2(), _global_globalStyles__WEBPACK_IMPORTED_MODULE_2__["siteColours"].blue, _global_globalStyles__WEBPACK_IMPORTED_MODULE_2__["transitions"].default, _global_globalStyles__WEBPACK_IMPORTED_MODULE_2__["siteColours"].blue);
});

var Button = function Button(props) {
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(StyledButton, {
    buttonStyle: props.buttonStyle,
    href: props.to,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: this
  }, props.children);
};

/* harmony default export */ __webpack_exports__["default"] = (Button);

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/define-properties.js":
/*!*********************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/define-properties.js ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/define-properties */ "core-js/library/fn/object/define-properties");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/core-js/object/freeze.js":
/*!**********************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/core-js/object/freeze.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! core-js/library/fn/object/freeze */ "core-js/library/fn/object/freeze");

/***/ }),

/***/ "./node_modules/@babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral.js":
/*!**********************************************************************************!*\
  !*** ./node_modules/@babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral.js ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _taggedTemplateLiteral; });
/* harmony import */ var _core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../core-js/object/define-properties */ "./node_modules/@babel/runtime-corejs2/core-js/object/define-properties.js");
/* harmony import */ var _core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _core_js_object_freeze__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../core-js/object/freeze */ "./node_modules/@babel/runtime-corejs2/core-js/object/freeze.js");
/* harmony import */ var _core_js_object_freeze__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_core_js_object_freeze__WEBPACK_IMPORTED_MODULE_1__);


function _taggedTemplateLiteral(strings, raw) {
  if (!raw) {
    raw = strings.slice(0);
  }

  return _core_js_object_freeze__WEBPACK_IMPORTED_MODULE_1___default()(_core_js_object_define_properties__WEBPACK_IMPORTED_MODULE_0___default()(strings, {
    raw: {
      value: _core_js_object_freeze__WEBPACK_IMPORTED_MODULE_1___default()(raw)
    }
  }));
}

/***/ }),

/***/ "./pages/index.jsx":
/*!*************************!*\
  !*** ./pages/index.jsx ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral */ "./node_modules/@babel/runtime-corejs2/helpers/esm/taggedTemplateLiteral.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! styled-components */ "styled-components");
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(styled_components__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_global_layout__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/global/layout */ "./components/global/layout.jsx");
/* harmony import */ var _components_global_globalStyles__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/global/globalStyles */ "./components/global/globalStyles.js");
/* harmony import */ var _components_home_wave__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/home/wave */ "./components/home/wave.jsx");
/* harmony import */ var _components_home_caption__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../components/home/caption */ "./components/home/caption.jsx");
/* harmony import */ var _static_logo_png__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../static/logo.png */ "./static/logo.png");
/* harmony import */ var _static_logo_png__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(_static_logo_png__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _components_home_socialLinks__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../components/home/socialLinks */ "./components/home/socialLinks.jsx");

var _jsxFileName = "/Users/giladgrant/Documents/Wadada/Internal/Development/wadada-design/pages/index.jsx";


function _templateObject() {
  var data = Object(_babel_runtime_corejs2_helpers_esm_taggedTemplateLiteral__WEBPACK_IMPORTED_MODULE_0__["default"])(["\n    #logo {\n        height: 30px;\n        opacity: 0;\n        animation: ", " 2s ", " ease forwards;\n\n        @media (max-width: 1600px) {\n            height: 20px;\n        }\n    }\n\n    #lhs, #rhs {\n        position: relative;\n        overflow: hidden;\n    }\n\n    #lhs {\n        height: 100%;\n        flex-basis: 60%;\n        background: white;\n        display: flex;\n        flex-direction: column;\n        justify-content: space-between;\n        flex-grow: 0;\n\n        @media (max-width: 1024px) {\n            flex-basis: 100%;\n            background: transparent;\n        }\n\n        ", " {\n            @media (max-width: 1600px) {\n                padding-top: 40px;\n                padding-bottom: 40px;\n            }\n\n            @media (max-width: 576px) {\n            justify-content: flex-start;\n\n            >*:not(:last-child) {\n                margin: 0 0 80px;\n            }\n        }\n        }\n    }\n\n    #rhs {\n        background: ", ";\n        flex-basis: 40%;\n\n        @media (max-width: 1024px) {\n            background: white;\n            flex-basis: 100%;\n            position: fixed;\n            top: 0;\n            left: 0;\n            width: 100%;\n            height: 100vh;\n            z-index: -1;\n        }\n    }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

//Packgae import


 // Other imports




 //Styles

var PageStyles = styled_components__WEBPACK_IMPORTED_MODULE_2___default.a.section(_templateObject(), _components_global_globalStyles__WEBPACK_IMPORTED_MODULE_4__["opacityReveal"], _components_global_globalStyles__WEBPACK_IMPORTED_MODULE_4__["transitions"].delay.second, _components_global_globalStyles__WEBPACK_IMPORTED_MODULE_4__["FlexContainer"], _components_global_globalStyles__WEBPACK_IMPORTED_MODULE_4__["siteColours"].blue);

var Home = function Home() {
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(PageStyles, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 76
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_global_layout__WEBPACK_IMPORTED_MODULE_3__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 77
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_global_globalStyles__WEBPACK_IMPORTED_MODULE_4__["FlexContainer"], {
    height: "100vh",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 78
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    id: "lhs",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 79
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_global_globalStyles__WEBPACK_IMPORTED_MODULE_4__["FlexContainer"], {
    flexDirection: "column",
    justifyContent: "space-between",
    height: "100%",
    padding: "60px 7.5%",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 80
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 81
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
    id: "logo",
    src: _static_logo_png__WEBPACK_IMPORTED_MODULE_7___default.a,
    alt: "Wadada, Design and Development by Gilad Grant",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 82
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_home_caption__WEBPACK_IMPORTED_MODULE_6__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 84
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_home_socialLinks__WEBPACK_IMPORTED_MODULE_8__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 85
    },
    __self: this
  }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    id: "rhs",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 88
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_home_wave__WEBPACK_IMPORTED_MODULE_5__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 89
    },
    __self: this
  })))));
};

/* harmony default export */ __webpack_exports__["default"] = (Home);

/***/ }),

/***/ "./static/logo.png":
/*!*************************!*\
  !*** ./static/logo.png ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASkAAABnCAMAAABrYkQYAAAAM1BMVEVMaXEAH/8AH/8AH/8AH/8AH/8AH/8AH/8AH/8AH/8AH/8AH/8AH/8AH/8AH/8AH/8AH/9z66G9AAAAEHRSTlMAMIBg8KDQQMAQ4CCwkHBQBbVCswAAAAlwSFlzAAAWJQAAFiUBSVIk8AAABL5JREFUeJzlnQuaoyAQhH2Lmof3P+1+0dlsJuxkpPi7zYx1AMQGqqsL0KIMGtqq7ofiSOjnDIxtPR0mVlmRuqG7HCRY0zlEL5+KUx81+zsxVE1urMb6KLE6Re+eiu4o8+qaPa3m00H4qswPVXOQJTh00aunT6tjKCwiVF0ZNfsbMeUvwLk5RqgArjIjq6FnkckT2YL9BptQIV17RBMuOfP/EjUowERZ4ZG6YbzoU6uNWkuHCVeZRGqem0qNFZEA58ZELAx9fSZ699zZS/SkbSBYfQ4WkVrDVecXXs9QpQ1CVVXULIcpv6B/gpqvkUGzlVX1GD0wD230iC0YiCHrTCNFGEWfESRqvUbtCLBcfzdMMF91UqjOUTvpsMl/jwCMouxQDQQNaEs/qZvstJJChcg8B2MPydN3SKEiRusUtcoDUX93KMsAyX8ezjpSU9xxjtr/HnXUSjrslPpjqPI34R6gSFCiAz6+OlHU/4VS3E9RK+kYo1ZNQIZKYfUqaiUd1vLzA2SoFKoCRJW9/FxBhkrIQ4SospefK0BaH4XRJUSV01YNKRaE9UfsarkoBUoAfkBYfwSpX6NWbVBGT5ahpGyA1JVlLwGsAYWUTZC6k1KAvNoFjVDcA09XHisB8YpWCCmbIHUPT2EBuCW4E6m7ndYjvNoVSsomSD1q1Ajg+hNGl9h+cCN1bv0pk4ooFNxOgHL5T3CMCPvFS6kjnV2hUMZPUupIZ1cIk4ooqbzsF7D+U3bBCU9dMcgkcJNKETcEqXuJKm5SKeRKJF83UcXJT2VwCffVS1Rx6W+3jVKvk+qcqa7IQML8MT5SdQfn6UlpiPCpvdYftv0gaRukonJaf8hJuQXSJjix+r3WH2YpSB1GSN1p/XGWuqQCkcf7rL8heq4KbWeXIHWn9ccJBaleRUjdp/7jHD3tYBMyUj71376czpC6z04px+kasxL2i8+mFsfpIl0g6tflTCNmqDdR05uA1Okul7o5nS4a24ijqF1RSQRm6KlsgZzo8jipxxl64hYco1QcqIoz9NQSDBkrD6rCvBfV12b2/h2oCpE0C9QVwKw/B1W1s07HuFK9e78d++78FdzZG/MCcN+dvwXM+rNn9X13/hYw89qc1TmhoJ/VYW4UmJ8V4gw9eVJBl1qttfo7TCpIrFgnwF1PU30AcjWMyxou/ekGJHVM11grcJNK34CDqMpaVe0v1H+KVUzt0mRJGiAHO9ypYQg1Uybnqyr74o8h9dx6IrsTLsfUAUc9v/DKZHWn+2zZLEFUqHnj5fTp69zb3Ewpn5MAvb4QkDn1Kc9DdzZc9rJW5IwnZw6pLOB26bbIkuqki6aFyvnr4Op4jmg3pV54f0ZdCxXNEEIv/P/4oISqxak0tRe7fJg/PVQW53fTkstOfzBIDFVjc3sz5SukjvLgM5LGs7PKzeVmIex2JzJGud2tMjw5P2yb3OOuP4Xa2Enrn8FsWYHyvwAo9BumlfwV/s0Yvitt2nf4c9a33wvntcF/UL44tNS8RZyWb6u/mlduvey/YIJT/U6/zLp+4RmPledoDpfnNNi1bxWmBUPdPs+ssXX7/sW/blyrUwhNF8KpurzvH26nvjqHELo5hFDVL9NdURR/AEKvIMPgA4TaAAAAAElFTkSuQmCC"

/***/ }),

/***/ 3:
/*!*******************************!*\
  !*** multi ./pages/index.jsx ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/giladgrant/Documents/Wadada/Internal/Development/wadada-design/pages/index.jsx */"./pages/index.jsx");


/***/ }),

/***/ "core-js/library/fn/object/define-properties":
/*!**************************************************************!*\
  !*** external "core-js/library/fn/object/define-properties" ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/define-properties");

/***/ }),

/***/ "core-js/library/fn/object/freeze":
/*!***************************************************!*\
  !*** external "core-js/library/fn/object/freeze" ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("core-js/library/fn/object/freeze");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-icons/fa":
/*!*********************************!*\
  !*** external "react-icons/fa" ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-icons/fa");

/***/ }),

/***/ "react-svg":
/*!****************************!*\
  !*** external "react-svg" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-svg");

/***/ }),

/***/ "styled-components":
/*!************************************!*\
  !*** external "styled-components" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-components");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map