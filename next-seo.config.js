const metaData = {
    name: 'Wadada Design',
    url: 'https://wadada-design.com',
    title: "Wadada Design | Web Design and Development by Gilad Grant",
    description: "Wadada! I'm Gil. I'm a web designer and developer based in Birmingham, England. I am passionate about clean design and creating modern web experiences... oh, and football. If you have a problem you need solving, I'm always free for a chat, especially if it's about websites.",
    image: 'OpenGraph.png'
}

export default {
    title: `${metaData.title}`,
    description: `${metaData.description}`,
    canonical: `${metaData.url}`,
    openGraph: {
        type: `website`,
        site_name: `${metaData.name}`,
        url: `${metaData.url}`,
        title: `${metaData.title}`,
        locale: `en_GB`,
        images: [{
            url: `${metaData.url}/static/${metaData.image}`,
            height: 1200,
            width: 1200,
            alt: `${metaData.name}`
        }]
    }
}