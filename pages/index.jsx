//Packgae import
import styled from 'styled-components'
import Layout from '../components/global/layout'
import { FlexContainer, opacityReveal, transitions, siteColours } from '../components/global/globalStyles'
// Other imports
import WaveSVG from '../components/home/wave'
import Caption from '../components/home/caption'
import logo from '../static/logo.png'
import SocialLinks from '../components/home/socialLinks';
//Styles
const PageStyles = styled.section`
    #logo {
        height: 30px;
        opacity: 0;
        animation: ${opacityReveal} 2s ${transitions.delay.second} ease forwards;

        @media (max-width: 1600px) {
            height: 20px;
        }
    }

    #lhs, #rhs {
        position: relative;
        overflow: hidden;
    }

    #lhs {
        height: 100%;
        flex-basis: 60%;
        background: white;
        display: flex;
        flex-direction: column;
        justify-content: space-between;
        flex-grow: 0;

        @media (max-width: 1024px) {
            flex-basis: 100%;
            background: transparent;
        }

        ${FlexContainer} {
            @media (max-width: 1600px) {
                padding-top: 40px;
                padding-bottom: 40px;
            }

            @media (max-width: 576px) {
            justify-content: flex-start;

            >*:not(:last-child) {
                margin: 0 0 80px;
            }
        }
        }
    }

    #rhs {
        background: ${siteColours.blue};
        flex-basis: 40%;

        @media (max-width: 1024px) {
            background: white;
            flex-basis: 100%;
            position: fixed;
            top: 0;
            left: 0;
            width: 100%;
            height: 100vh;
            z-index: -1;
        }
    }
`;


const Home = () => (
    <PageStyles>
        <Layout>
            <FlexContainer height="100vh">
                <div id="lhs">
                    <FlexContainer flexDirection="column" justifyContent="space-between" height="100%" padding="60px 7.5%">
                        <span>
                            <img id="logo" src={logo} alt="Wadada, Design and Development by Gilad Grant" />
                        </span>
                        <Caption />
                        <SocialLinks />
                    </FlexContainer>
                </div>
                <div id="rhs">
                    <WaveSVG />
                </div>
            </FlexContainer>
        </Layout>
    </PageStyles>
);

export default Home;

