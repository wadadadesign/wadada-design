import Head from 'next/head'
import GlobalStyles from '../global/globalStyles'

const Layout = (props) => (
    <>
        <Head>
            <meta charSet="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel="stylesheet" href="https://use.typekit.net/ihj0vor.css" />
            <link rel="manifest" href="/static/manifest.json" />
            <link rel="shortcut icon" href="/static/favicon.ico" />
        </Head>
        <main>
            <GlobalStyles />
            {props.children}
        </main>
    </>
);

export default Layout;