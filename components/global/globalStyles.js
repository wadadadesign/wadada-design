import styled, { createGlobalStyle, keyframes } from "styled-components"

//Site wide variables
export const siteColours = {
    black: "#111",
    blue: "#0f1dff",
    lightGrey: "#525252"
}

export const siteFonts = {
    display: "objektiv-mk1, sans-serif;"
}

export const transitions = {
    default: "0.3s ease",
    cubicBezier: {
        default: "cubic-bezier(.9,0,.7,1)",
        soft: "cubic-bezier(.33,.31,.21,1)"
    },
    delay: {
        first: "1s",
        second: "2s",
        third: "3s",
        fourth: "4s"
    }
}

export const opacityReveal = keyframes`
    from {opacty: 0;}
    to {opacity: 1;}
`;

export const titleReveal = keyframes`
    from {transform: translateY(20px); opacity: 0;}
    to {transfrom: translateY(0); opacity: 1;}
`;


export const FlexContainer = styled.section`
    display: flex;
    overflow: hidden;
    position: relative;
    height: ${props => props.height || "auto"};
    flex-direction: ${props => props.flexDirection || "flex-start"};
    justify-content: ${props => props.justifyContent || "flex-start"};
    padding: ${props => props.padding || "none"};

    @media (max-width: 576px) {
        height: auto;
    }
`;

//Gloabl styles
export default createGlobalStyle`
body {
    height: 100vh;
    overflow: hidden;
    position: relative;
    color: ${siteColours.black};
    margin: 0;
    font-family: ${siteFonts.display};
    font-weight: 400;
    font-size: 21px;

    @media (max-width: 576px) {
        overflow: scroll;
    }
}

h1, h2, h3, h4, h5 {
  font-weight: 500;
}

h1 {
    font-size: 3.5vw;

    @media (max-width: 1024px) {
        font-size: 5.5vw;
    }

    @media (max-width: 576px) {
        font-size: 32px;
    }
}

h2 {
    font-size: 2.6vw;

    @media (max-width: 1024px) {
        font-size: 3.5vw;
    }

    @media (max-width: 576px) {
        font-size: 24px;
        line-height: 1.5;
    }
}


h1, h2, h3, h4, h5, p {
    margin: 0 0 40px;

    @media (max-width: 1200px) {
        margin: 0 0 25px;
    }
}

p {
    color: ${siteColours.lightGrey};
    line-height: 1.75;
    font-size: 1vw;

    @media (max-width: 1024px) {
        font-size: 2vw;
    }

    @media (max-width: 576px) {
        font-size: 21px;
    }
}

button, input, textarea, select {
    font-family: ${siteFonts.display}
}
`;