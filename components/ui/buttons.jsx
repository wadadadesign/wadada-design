import { siteColours, transitions } from "../global/globalStyles"
import styled, { css } from 'styled-components'

const StyledButton = styled.a`
    font-size: 0.8vw;
    text-transform: uppercase;
    padding: 20px 40px;
    border-radius: 50px;
    cursor: pointer;
    font-weight: 500;
    border: none;
    box-shadow: 0 5px 10px 0 rgba(7, 40, 251, 0.21);
    display: inline-block;
    text-decoration: none;

    @media (max-width: 1024px) {
        font-size: 12px;
        padding: 15px 30px;
    }

    ${props => props.buttonStyle == 'primary' && css`
        background: white;
        color: ${siteColours.blue};
        transition: ${transitions.default};

        :hover {
            background: ${siteColours.blue};
            color: white;
            transform: translateY(3px);
        }
    `}
`;

const Button = (props) => (
    <StyledButton buttonStyle={props.buttonStyle} href={props.to}>
        {props.children}
    </StyledButton >
);

export default Button