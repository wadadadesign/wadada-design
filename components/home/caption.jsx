import Button from '../ui/buttons'
import styled from 'styled-components'
import { transitions, titleReveal } from '../global/globalStyles'

const CaptionContainer = styled.div`
    padding-right: 10%;

    @media (max-width: 1024px) {
        padding: 0;
    }
`;

const AnimatedTag = styled.span`
opacity: 0;
display: inline-block;
animation: ${titleReveal} 1s ease 1 forwards;
`;

const AnimatedTagDelayed = styled(AnimatedTag)`
animation-delay: ${transitions.delay.first};
`;

const Caption = () => (
    <CaptionContainer>
        <h1><AnimatedTag>Wadada! I'm Gil</AnimatedTag></h1>
        <AnimatedTagDelayed>
            <h2>I'm a web designer and developer based in Birmingham, UK</h2>
            <p>I am passionate about clean design and creating modern web experiences... oh, and football. If you have a problem you need solving, I'm always free for a chat, especially if it's about websites.</p>
            <Button buttonStyle="primary" to="mailto:gilad@wadada-design.com?subject=What a lovely site you have!">Let's chat</Button>
        </AnimatedTagDelayed>
    </CaptionContainer>
);

export default Caption;