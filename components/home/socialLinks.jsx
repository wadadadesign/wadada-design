import { FaDribbble, FaInstagram } from 'react-icons/fa'
import styled from 'styled-components'
import { siteColours, transitions, opacityReveal } from '../global/globalStyles'

const socialLinks = {
    dribble: "https://dribbble.com/wadadadesign",
    instagram: "https://instagram.com/wadadadesign"
}

const SocialLinksContainer = styled.div`
display: flex;
opacity: 0;
animation: ${opacityReveal} 2s ${transitions.delay.third} ease forwards;
        
>*:not(:last-of-type) {
    margin-right: 30px;
}

svg {
    color: ${siteColours.blue}
    height: 1.5em;
    width: auto;

    @media (max-width: 1600px) {
        height: 20px;
    }
}
`;

const SocialLinks = () => (
    <SocialLinksContainer>
        <a href={socialLinks.dribble} rel="noopener" target="_blank" title="Check me out on Dribbble"><FaDribbble /></a>
        <a href={socialLinks.instagram} rel="noopener" target="_blank" title="Check me out on Instagram"><FaInstagram /></a>
    </SocialLinksContainer>
);

export default SocialLinks;