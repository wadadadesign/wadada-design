import styled, { css, keyframes } from "styled-components"
import ReactSVG from 'react-svg'
import { transitions, opacityReveal, siteColours } from '../global/globalStyles'

//Wave animation
const waveMove = keyframes`
0% {transform: translate3D(0%, 0, 0);}
50% {transform: translate3D(-150%, 0, 0);}
100% {transform: translate3D(0, 0, 0);}
`;

//White overlay animation
const overlayReveal = keyframes`
    from {
        width: 100%;
    }
    to {
        width: 0%;
    }
`;

//Create delay in wave animation
const waveAnimationDelay = () => {
    let styles = '';
    //Loop and increase animation delay by 0.1 each time
    for (let i = 1; i < 30; i++) {
        styles += `
            path:nth-of-type(${i}) {
                animation-delay: ${i * 0.1}s;
            }
        `
    }
    //`css` function 
    return css`${styles}`;
}

//Styles for the SVG
const OuterSVG = styled.div`
position: relative;
height: 100%;

&::after {
    content: '';
    position: absolute;
    top: 0;
    right: 0;
    width: 100%;
    height: 100%;
    z-index: 10;
    background: white;
    animation: ${overlayReveal} .5s ${transitions.cubicBezier.default} ${transitions.delay.first} 1 forwards;
}

svg {
    position: absolute;
    transform: rotate(30deg) scale(2);
    pointer-events: none;
    height: 100%;
    opacity: 0;
    animation: ${opacityReveal} 3s ease ${transitions.delay.second} 1 forwards;

    path {
      animation: ${waveMove} 20s ease-in-out infinite alternate;
      stroke: white;
      stroke-width: 10 !important;

      @media (max-width: 1024px) {
          opacity: 0.01;
        stroke: ${siteColours.blue};
        }
    }

    ${waveAnimationDelay()}
  }
`;

//WaveSVG component
const WaveSVG = () => (
    <OuterSVG >
        <ReactSVG src="../static/Test.svg" />
    </OuterSVG >
);

export default WaveSVG;